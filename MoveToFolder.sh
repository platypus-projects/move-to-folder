#!/bin/sh

echo "Processing files"

for i in "$@" ; do
  if [ ! -f "$i" ] ; then
    echo "skipping: $i (not a file)"
    continue
  fi

  DIRNAME=$( dirname "$i" )
  BASENAME=$( basename "$i" )
  SUBDIR=${BASENAME:0:8}

  if [ ! -e "$DIRNAME/$SUBDIR" ] ; then
    echo "creating: $DIRNAME/$SUBDIR"
    mkdir "$DIRNAME/$SUBDIR"
  fi

  if [ -d "$DIRNAME/$SUBDIR" ] ; then
    echo "moving $i to $SUBDIR/"
    mv -n "$i" "$DIRNAME/$SUBDIR/$BASENAME"
  fi
done

echo "Done."
